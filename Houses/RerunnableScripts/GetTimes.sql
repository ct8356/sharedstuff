IF OBJECT_ID(N'dbo.GetTimes', N'TF') IS NOT NULL
    DROP FUNCTION dbo.GetTimes;
GO
CREATE FUNCTION dbo.GetTimes
(
	@lifestyleId INT
)
RETURNS @times TABLE 
(
    -- Columns returned by the function
    TimeSpentTravellingToWorkPerYearInHours DECIMAL(18,1)
	, TimeSpentTravellingToNottinghamPerYearInHours DECIMAL(18,1)
	, TimeSpentTravellingToParentsPerYearInHours DECIMAL(18,1)
)
AS BEGIN
    INSERT @times
    SELECT 
		h.RoundTripToWorkInMinutes 
		* ls.TripsToWorkPerYear  / 60
		, (h.RoundTripToNearestStationInMinutes + s.TimePerRoundTripToNottinghamInMinutes)
		* ls.TripsToNottinghamPerYear  / 60
		, (h.RoundTripToNearestStationInMinutes + s.TimePerRoundTripToParentsInMinutes)
		* ls.TripsToParentsPerYear  / 60
	FROM Houses h
	JOIN Locations l ON l.ID = h.LocationId
	JOIN Stations s ON s.ID = h.NearestStationId
	JOIN Lifestyles ls ON ls.ID = @lifestyleId;
    RETURN;
END;
GO
