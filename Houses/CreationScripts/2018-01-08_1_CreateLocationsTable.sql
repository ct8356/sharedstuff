DROP TABLE IF EXISTS Locations;
CREATE TABLE Locations (
	--General
	ID INT IDENTITY,
	Name NVARCHAR(50) NOT NULL,
	CouncilTaxPerYearInPounds INT NOT NULL,
	AverageRentPerRoomPerMonthIncludingBillsInPounds INT NOT NULL,
	AverageHousePriceInPounds INT NOT NULL,
	EstimatedYearlyIncreaseInHouseValueInPercent INT NOT NULL,
	PRIMARY KEY(ID)
);