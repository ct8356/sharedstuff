DROP TABLE IF EXISTS Houses;
CREATE TABLE Houses (
	--General
	ID INT IDENTITY,
	FirstGlanceScore INT,
	Comments NVARCHAR(MAX),
	Link NVARCHAR(MAX),
	--Location
	HouseNumber NVARCHAR(50) NOT NULL,
	StreetName NVARCHAR(50) NOT NULL,
	LocationId INT NOT NULL,
	--Travel
	NearestStationId INT NOT NULL,
	RoundTripToNearestStationInMinutes INT NOT NULL,
	RoundTripToWorkInMinutes INT NOT NULL,
	--Price
	AskingPriceInPounds INT NOT NULL,
	PossiblePriceInPounds INT,
	--Bedrooms
	BedroomCount INT NOT NULL, --Assuming one dining room/large kitchen, no living room.
	LoftRoomPossible BIT,
	CostOfInstallingLoftRoomInPounds INT,
	--Parking
	OffroadParkingCount INT NOT NULL,
	--Recreation
	HasDanceSpace BIT NOT NULL,
	DanceSpaceScore INT,
	CostOfInstallingDanceSpaceInPounds INT NOT NULL,
	--Energy
	CurrentEnergyEfficiencyRating INT,
	PotentialEnergyEfficiencyRating INT,
	CurrentEnvironmentalImpactRating INT,
	PotentialEnvironmentalImpactRating INT,
	--Constraints
	PRIMARY KEY(ID),
	CONSTRAINT UQ_House UNIQUE (HouseNumber, StreetName, LocationId),
	CONSTRAINT FK_House_Location FOREIGN KEY(LocationId) REFERENCES Locations(Id),
	CONSTRAINT FK_House_NearestStation FOREIGN KEY(NearestStationId) REFERENCES Stations(Id)
);