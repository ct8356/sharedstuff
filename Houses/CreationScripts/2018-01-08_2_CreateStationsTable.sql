DROP TABLE IF EXISTS Stations;
CREATE TABLE Stations (
	--General
	ID INT IDENTITY,
	Name NVARCHAR(50),
	TimePerRoundTripToNottinghamInMinutes INT,
	TimeBetweenServicesToNottinghamInMinutes INT,
	CostPerRoundTripToNottinghamInPounds DECIMAL(18,2),
	TimePerRoundTripToParentsInMinutes INT,
	CostPerRoundTripToParentsInPounds INT,
	EntryLastUpdated DATE,
	PRIMARY KEY(ID)
);