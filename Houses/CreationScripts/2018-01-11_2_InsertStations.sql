INSERT INTO Stations( 
Name
, TimePerRoundTripToNottinghamInMinutes 
, TimeBetweenServicesToNottinghamInMinutes
, CostPerRoundTripToNottinghamInPounds
, TimePerRoundTripToParentsInMinutes
, CostPerRoundTripToParentsInPounds
, EntryLastUpdated)
VALUES
('Beeston train station'			, 16,  20, 2.70,	480, 80, 2018-01-13),
('A52 bus station'					, 40,  10, 4,		480, 80, 2010-01-13),
('Bramcote Lane bus station'		, 40,  10, 3.70,	480, 80, 2018-01-13),
('A609 bus station'					, 40,  10, 4,		480, 80, 2010-01-13),
('Langley Mill train station'		, 40,  60, 7.40,	480, 80, 2018-01-13),
('Bulwell train station'			, 26, 110, 3.00,	480, 80, 2018-01-13),
('Ilkeston train station'			, 30,  30, 5.50,	480, 80, 2018-01-13),
('Longeaton train station'			, 28,  30, 4.90,	480, 80, 2018-01-13);

