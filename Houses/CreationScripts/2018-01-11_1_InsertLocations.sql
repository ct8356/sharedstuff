INSERT INTO Locations
(Name
,CouncilTaxPerYearInPounds
,AverageRentPerRoomPerMonthIncludingBillsInPounds
,AverageHousePriceInPounds
,EstimatedYearlyIncreaseInHouseValueInPercent)
VALUES
('Beeston'		, 1200, 320, 160000, 4),
('Wollaton'		, 1200, 320, 160000, 4),
('Eastwood'		, 1200, 320, 160000, 4),
('Ilkeston'		, 1200, 320, 160000, 4),
('Bulwell'		, 1200, 320, 160000, 4),
('Bath'			, 2000, 600, 250000, 3);

