DECLARE @id INT;
DECLARE @locationId INT;
DECLARE @stationId INT;


INSERT INTO Houses(
HouseNumber, StreetName, LocationId, 
NearestStationId, RoundTripToNearestStationInMinutes, RoundTripToWorkInMinutes,
AskingPriceInPounds,
BedroomCount,
OffroadParkingCount,
HasDanceSpace, CostOfInstallingDanceSpaceInPounds
)
VALUES(
'11', 'East Crescent', 1,
1, 30, 60,
240000,
5,
3,
1, 200
);
SELECT @id = SCOPE_IDENTITY();
UPDATE Houses SET 
CurrentEnergyEfficiencyRating = 54, 
PotentialEnergyEfficiencyRating = 67,
CurrentEnvironmentalImpactRating = 48, 
PotentialEnvironmentalImpactRating =61
WHERE id = @id;


INSERT INTO Houses(
HouseNumber, StreetName, LocationId,
NearestStationId, RoundTripToNearestStationInMinutes, RoundTripToWorkInMinutes,
AskingPriceInPounds,
BedroomCount,
OffroadParkingCount,
HasDanceSpace, CostOfInstallingDanceSpaceInPounds
)  
VALUES(
'30', 'Victory Road', 1,
1, 20, 60,
160000,
3,
0,
1, 2000
);
SELECT @id = SCOPE_IDENTITY();
UPDATE Houses SET 
CurrentEnergyEfficiencyRating = 40, 
PotentialEnergyEfficiencyRating = 77,
CurrentEnvironmentalImpactRating = 34, 
PotentialEnvironmentalImpactRating =71
WHERE id = @id;