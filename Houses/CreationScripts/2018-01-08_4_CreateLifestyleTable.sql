DROP TABLE IF EXISTS Lifestyles;
CREATE TABLE Lifestyles (
	--General
	ID INT IDENTITY,
	Name NVARCHAR(50),
	TripsToWorkPerYear INT,
	TripsToNottinghamPerYear INT,
	TripsToParentsPerYear INT,
	HourlyRateInPounds INT,
	PRIMARY KEY(ID)
);